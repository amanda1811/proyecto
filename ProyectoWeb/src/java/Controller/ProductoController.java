/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import EJB.ProductoFacadeLocal;
import Entity.Categoria;
import Entity.Producto;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class ProductoController implements Serializable {

    @EJB
    private ProductoFacadeLocal productoFacade;
    private List<Producto> listaProducto;
    private Producto producto;
    private Categoria categoria;
    String mensaje = "";

    public List<Producto> getListaProducto() {
        this.listaProducto = productoFacade.findAll();
        return listaProducto;
    }

    public void setListaProducto(List<Producto> listaProducto) {
        this.listaProducto = listaProducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @PostConstruct
    public void init() {
        producto = new Producto();
        categoria = new Categoria();
    }

    public void crear() {
        try {
            producto.setIdCategoria(categoria);
            productoFacade.create(producto);
            mensaje = "Producto Creado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void editar() {
        try {
            producto.setIdCategoria(categoria);
            productoFacade.edit(producto);
            mensaje = "Producto Editado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminar(Producto po) {
        try {
            productoFacade.remove(po);
            mensaje = "Producto Eliminado";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargar(Producto po) {
        producto = po;
    }    

}
