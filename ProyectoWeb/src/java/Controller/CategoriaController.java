/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import EJB.CategoriaFacadeLocal;
import Entity.Categoria;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class CategoriaController implements Serializable {

    @EJB
    private CategoriaFacadeLocal categoriaFacade;
    private List<Categoria> listaCategoria;
    private Categoria categoria;
    String mensaje = "";

    public List<Categoria> getListaCategoria() {
        this.listaCategoria = categoriaFacade.findAll();
        return listaCategoria;
    }

    public void setListaCategoria(List<Categoria> listaCategoria) {
        this.listaCategoria = listaCategoria;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @PostConstruct
    public void init() {
        categoria = new Categoria();
    }

    public void crear() {
        try {
            categoriaFacade.create(categoria);
            mensaje = "Categoria Creada";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void editar() {
        try {
            categoriaFacade.edit(categoria);
            mensaje = "Categoria Editada";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void eliminar(Categoria cat) {
        try {
            categoriaFacade.remove(cat);
            mensaje = "Categoria Eliminada";
        } catch (Exception e) {
            mensaje = "Error:" + e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }

    public void cargar(Categoria cat) {
        categoria = cat;
    }
}
